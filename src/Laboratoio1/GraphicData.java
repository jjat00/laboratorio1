package Laboratoio1;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.XYChart;

public class GraphicData {
    private XYChart chart;
    
    public XYChart crearGrafica(double[] xData, double[] yData) {
        chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
        return chart;
    }
}
