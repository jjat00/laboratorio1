package Laboratoio1;

import java.awt.BorderLayout;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.BoxLayout;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;

import javax.swing.JButton;
import javax.swing.JComboBox;;

public class GUILaboratorio1 extends JFrame{
    private Container containerVentanaLab;

    private JPanel panelSeñalesAnalogicas;
    private JPanel panelButtons;
    private JPanel chartPanel; 
    private JButton botonGraficar;
    private JButton botonAlmacenar;
    private JComboBox comboBoxADC;

    private JPanel panelSeñalesDigitales;
    private JPanel panelBotones;
    private JPanel panelGrafica;
    private JButton botonGraficarDigital;
    private JButton botonAlmacenarSeñalDigital;
    private GraphicData graficaChart;
    private XYChart chart;

	public GUILaboratorio1() {
        super("Laboratio 1 Interfaces");
        
        inicializarVentana();
        
        // Set default window configuration
        this.setUndecorated(false);
        this.setPreferredSize(new Dimension(500,500));
        // Display the window.
        this.pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }
    
    public void inicializarVentana() {
        containerVentanaLab = getContentPane();
        JPanel ventanaPrincipal = new JPanel();
        ventanaPrincipal.setLayout(new BoxLayout(ventanaPrincipal, BoxLayout.Y_AXIS));
        ventanaPrincipal.add(señalesAnalogicas());
        ventanaPrincipal.add(señalesDigitales());
        containerVentanaLab.add(ventanaPrincipal);
    }

    public JPanel señalesAnalogicas() {
        panelSeñalesAnalogicas = new JPanel();
        panelSeñalesAnalogicas.setLayout(new BorderLayout());

        panelButtons = new JPanel();
        // panelButtons.setLayout(new BoxLayout(panelButtons, BoxLayout.Y_AXIS) );
        panelButtons.setLayout(new FlowLayout(FlowLayout.CENTER));

        botonGraficar = new JButton("Graficar");
        botonAlmacenar = new JButton("Guardar");

        String[] temas = { "ADC1", "ADC2", "ADC3", "ADC4", "ADC5", "ADC6", "ADC7", "ADC8" };
        comboBoxADC = new JComboBox<>(temas);
        // comboBoxADC.addActionListener(escuchaComboBox);

        panelButtons.add(comboBoxADC);
        panelButtons.add(botonGraficar);
        panelButtons.add(botonAlmacenar);

        chartPanel = new XChartPanel<XYChart>(graficarDatos());

        panelSeñalesAnalogicas.add(panelButtons, BorderLayout.NORTH);
        panelSeñalesAnalogicas.add(chartPanel, BorderLayout.SOUTH);
        return panelSeñalesAnalogicas;
    }

    public JPanel señalesDigitales() {
        panelSeñalesDigitales = new JPanel();
        panelSeñalesDigitales.setLayout(new BorderLayout());

        panelBotones = new JPanel();
        panelBotones.setLayout(new FlowLayout(FlowLayout.CENTER));

        botonGraficarDigital = new JButton("Graficar");
        botonAlmacenarSeñalDigital = new JButton("Guardar");

        panelBotones.add(botonGraficarDigital);
        panelBotones.add(botonAlmacenarSeñalDigital);

        panelGrafica = new XChartPanel<XYChart>(graficarDatos());

        panelSeñalesDigitales.add(panelBotones, BorderLayout.NORTH);
        panelSeñalesDigitales.add(panelGrafica, BorderLayout.SOUTH);
        return panelSeñalesDigitales;
    }

    public XYChart graficarDatos() {
        double[] xData = new double[] { 0.0, 1.0, 2.0 };
        double[] yData = new double[] { 2.0, 1.0, 0.0 };
        graficaChart = new GraphicData();
        chart = graficaChart.crearGrafica(xData, yData);
        return chart;
    }



}